# nmap automate

## Options

1. **Quick**: Shows all open ports quickly (~15 seconds)
2. **Basic**: Runs Quick Scan, then runs a more thorough scan on found ports (~5 minutes)
3. **UDP**: Runs "Basic" on UDP ports (~5 minutes)
4. **Full**: Runs a full range port scan, then runs a thorough scan on new ports (~5-10 minutes)
5. **Vulns**: Runs CVE scan and nmap Vulns scan on all found ports (~5-15 minutes)
6. **Recon**: Runs "Basic" scan "if not yet run", then suggests recon commands "i.e. gobuster, nikto, smbmap" based on the found ports, then prompts to automatically run them
7. **All**: Runs all the scans consecutively (~20-30 minutes)


# Requirements:

Gobuster

`apt install gobuster`

# Example:

`./nmap-automate.sh <TARGET-IP> <TYPE>`
`./nmap-automate.sh 10.1.1.1 All `